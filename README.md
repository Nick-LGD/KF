1. Composer install
2. Composer update
3. php bin/console doctrine:database:create
4. php bin/console make:migration
5. php bin/console doctrine:migrations:migrate

Technologie utiliser :

- Symfony
- PHP
- JavaScript
- Twig
- HTML / CSS
